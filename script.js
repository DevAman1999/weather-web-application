let dates = null;
let weatherCondition = null;
fetch(
  "https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/germany?unitGroup=metric&key=L7JEPHW7GXHCBFY63ZZ6932AX&contentType=json"
)
  .then((response) => {
    if (!response.ok) throw new Error("failed to fetch the data");
    return response.json();
  })
  .then((data) => {
    dates = data.days;
    weatherCondition = data.currentConditions;
    weathercondition();
    populateWeatherdetails();
  })
  .catch((error) => {
    console.log("could load site", error);
  });

const dropdownMenu = document.getElementById("stations");
const dropdownButton = document.getElementById("dropdownMenuButton");
const weatherdetails = document.getElementById("details");
function populateDropdown() {
  dropdownMenu.innerHTML = "";
  for (let day of dates) {
    const station = day.datetime;
    const option = document.createElement("a");
    option.className = "dropdown-item";
    option.href = "#";
    option.textContent = station;
    console.log(option.textContent);
    dropdownMenu.appendChild(option);
    option.addEventListener("click", function () {
      showdetails(day);
    });
  }
  dropdownButton.textContent = "Select a date";
}
function showdetails(day) {
  console.log("Hi: " + day.datetime);
  document.getElementById("modalContainer").innerHTML = `
    <div class="modal" tabindex="-1" id="stationsModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">${day.datetime}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" style = "text-align: start;">
             Temperature: ${day.temp}°C <br>
              Maximum Temperature: ${day.tempmax}°C <br>
              Minimum Temperature: ${day.tempmin}°C <br>
              Humidity: ${day.tempmin} g/m(gram/meter) <br>
              Wind Speed: ${day.windspeed} km/h <br>
              Pressure: ${day.pressure} Pa(Pascal) <br>
         </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
  `;

  // Initialize the modal (if Bootstrap's JavaScript is included)
  var modal = new bootstrap.Modal(document.getElementById("stationsModal"));
  modal.show();
}
function weathercondition() {
  const weatherData = weatherCondition.temp;
  console.log(weatherData);
  document.getElementById(
    "weather-text"
  ).innerHTML = `Today Weather : ${weatherData} °C&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${
    weatherCondition.icon
  } 
    <div style = "display:flex ;margin-left:290px;font-size:20px">
    ${weatherCondition.datetime.substring(0, 5)}
    </div>`;
}
function populateWeatherdetails() {
  weatherdetails.innerHTML = "";
  for (let day of dates) {
    const details = `
    <nav class="navbar bg-body-tertiary">
    <div class="container-fluid">
      <span class="navbar-text">
      <img src="images/cloudy.png" alt="" style="height:2em;width:2em;">
      <div style = "display:inline-block;font-size:20px; text-align:center;">
      <p style ="word-spacing:800px;">
      ${day.datetime}
      ${day.temp}°C
      </p>
      </div>
      <div style ="text-align:start;">
      ${day.description}
      </div>
      </span>
    </div>
  </nav>
`;
    const station = day.datetime;
    const option = document.createElement("div");
    option.className = "weather-container";

    option.innerHTML = details;
    console.log(option.textContent);
    weatherdetails.appendChild(option);
  }
}
